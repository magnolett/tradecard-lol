package br.com.tradecard.lol.ui;

import java.io.IOException;

import javax.swing.JWindow;

import br.com.tradecard.lol.dto.Battlefield;

public class MainUI extends JWindow {
	private static final long serialVersionUID = -628450154943172768L;

	public static void main(String[] args) throws IOException {
		new Battlefield().createWindow();
	}
}
