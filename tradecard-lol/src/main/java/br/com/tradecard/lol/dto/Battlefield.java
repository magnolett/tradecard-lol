package br.com.tradecard.lol.dto;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

public class Battlefield extends JWindow {

	private String pathDefault = "target/start.jpg";
	private static final long serialVersionUID = 9213262038540510027L;
	//private static final Image startImage = Toolkit.getDefaultToolkit().getImage("/start.jpg");
	JPanel firstFrame;
	JButton batalhar;
	JButton cards;

	public void createWindow() throws IOException {
		setSize(1000, 800);
		setVisible(true);
		setLocationRelativeTo(null);
		buildFirstComponents();
	}
	
	public void buildFirstComponents() throws IOException {
		
//	    BufferedImage image = ImageIO.read(new File("C:/Users/marco/git/repository/tradecard-lol/src/main/resources/start.jpg"));
//	    JLabel label = new JLabel(new ImageIcon(image));
//	    label.setSize(1000, 800);
		firstFrame = new JPanel();
		firstFrame.setSize(1000, 800);
		firstFrame.setVisible(true);
		
		JLabel imageLabel = new JLabel();
		imageLabel.setSize(this.getSize());
		
		BufferedImage read = ImageIO.read(new FileInputStream(pathDefault));
		ImageIcon imageIcon = new ImageIcon(read);
		imageLabel.setIcon(imageIcon);
		
		batalhar = new JButton("Batalhar!");
		batalhar.setSize(100, 50);
		batalhar.setVisible(true);
		batalhar.setLocation(450, 550);
		
		batalhar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				firstFrame.setVisible(false);
				try {
					add(buildBattlefield());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		firstFrame.add(imageLabel);
		firstFrame.add(batalhar);
//		firstFrame.add(label);
		this.add(firstFrame);
		
	}
	
	public JPanel buildBattlefield() throws IOException {

		JPanel battlePanel = new JPanel();
		battlePanel.setSize(this.getSize());
		battlePanel.setVisible(true);
		
		JButton attack = new JButton("Atacar!");
		attack.setSize(300, 200);
		
		battlePanel.add(attack);
		
		
		return battlePanel;
	}
	
//	public void paintImage(Graphics g) {
//		g.drawImage(startImage, 500, 500, 1000, 800, null);
		
	
}
