package br.com.tradecard.lol.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class StartUI extends JPanel {
	
	private static final long serialVersionUID = 2279877151928470170L;

	private String imagePath = "start.jpg";
    private JComboBox<String> jcomp1;
    private JLabel jcomp2;
    private JLabel jcomp3;
    private JButton jcomp4;

    public StartUI() throws FileNotFoundException, IOException {
        String[] jcomp1Items = {"Selecione...", "Ionia", "Shurima", "Demacia", "Noxus"};

        jcomp1 = new JComboBox<String> (jcomp1Items);
        jcomp2 = new JLabel (" Selecione um dos decks abaixo");
        jcomp3 = new JLabel (" e prepare-se para a BATALHA!");
        jcomp4 = new JButton ("CONFIRMAR");

		JLabel imageLabel = new JLabel();
		imageLabel.setSize(new Dimension (944, 574));
//		ImageIcon imageIcon = new ImageIcon(imagePath);
		ImageIcon imageIcon = new ImageIcon(getClass().getClassLoader().getResource(imagePath));
		imageLabel.setIcon(imageIcon);
        
        setPreferredSize (new Dimension (944, 574));
        setLayout (null);

        add (jcomp1);
        add (jcomp2);
        add (jcomp3);
        add (jcomp4);
        add(imageLabel);
        
        jcomp1.setOpaque(true);
        jcomp2.setOpaque(true);
        jcomp3.setOpaque(true);
        jcomp4.setOpaque(true);

        jcomp1.setBounds (395, 365, 140, 25);
        jcomp2.setBounds (370, 85, 190, 25);
        jcomp3.setBounds (370, 105, 190, 25);
        jcomp4.setBounds (395, 405, 140, 25);
        
        jcomp4.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				enterTheBattle();
			}
		});
    }
    
    public void enterTheBattle() {
    	if(jcomp1.getSelectedItem().equals("Selecione...")) {
    		JOptionPane.showMessageDialog(null, "Voc� n�o selecionou nenhum deck de cartas", "ERRO", JOptionPane.ERROR_MESSAGE);
    	} else {
    		try {
				new BattleUI().createScreen(jcomp1.getSelectedItem().toString());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    }

    public static void main (String[] args) throws FileNotFoundException, IOException {
        JFrame frame = new JFrame ("Hora da batalha!");
        frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add (new StartUI());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible (true);
    }
}
