package br.com.tradecard.lol.ui;

import br.ufsc.inf.leobr.cliente.Jogada;
import br.ufsc.inf.leobr.cliente.OuvidorProxy;
import br.ufsc.inf.leobr.cliente.Proxy;
import br.ufsc.inf.leobr.cliente.exception.ArquivoMultiplayerException;
import br.ufsc.inf.leobr.cliente.exception.JahConectadoException;
import br.ufsc.inf.leobr.cliente.exception.NaoConectadoException;
import br.ufsc.inf.leobr.cliente.exception.NaoJogandoException;
import br.ufsc.inf.leobr.cliente.exception.NaoPossivelConectarException;

public class AtorRede implements OuvidorProxy{

	private static final long serialVersionUID = 4040763441773371449L;
	private BattleUI battleUI;
	private Proxy proxy;
	
	private boolean meuTurno = false;
	
	public AtorRede(BattleUI battleUI) {
		this.battleUI = battleUI;
		proxy = Proxy.getInstance();
		proxy.addOuvinte(this);
	}
	
	public void conectar(String nome, String servidor) {
		try {
			proxy.conectar(servidor, nome);
		} catch (JahConectadoException e) {
			e.printStackTrace();
		} catch (NaoPossivelConectarException e) {
			e.printStackTrace();
		} catch (ArquivoMultiplayerException e) {
			e.printStackTrace();
		}
	}
	
	public void iniciarPartidaRede() {
		try {
			proxy.iniciarPartida(2);
		} catch (NaoConectadoException e) {
			e.printStackTrace();
		}
	}
	
	public void enviarJogada(String mensagem) {
		Message message = new Message(mensagem);
		try {
			proxy.enviaJogada(message);
		} catch (NaoJogandoException e) {
			e.printStackTrace();
		}
	}
	
	public void iniciarNovaPartida(Integer posicao) {
		meuTurno = posicao == 1 ? true : false;
		battleUI.iniciarPartidaRede(meuTurno);
	}
	
	public void finalizarPartidaComErro(String message) {
	}

	public void receberMensagem(String msg) {
	}

	public void receberJogada(Jogada jogada) {
		Message message = (Message) jogada;
		battleUI.receberMensagemRede(message.getMensagem());
	}

	public void tratarConexaoPerdida() {
	}

	public void tratarPartidaNaoIniciada(String message) {
	}
	
	public void desconectar() {
		try {
			proxy.desconectar();
		} catch (NaoConectadoException e) {
			e.printStackTrace();
		}
	}
	
	public String obterNomeAdversario() throws Exception {
		return proxy.obterNomeAdversario(2) != null ? proxy.obterNomeAdversario(2) : "Nenhum";
	}
}
