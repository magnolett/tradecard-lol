package br.com.tradecard.lol.ui;

import br.ufsc.inf.leobr.cliente.Jogada;

public class Message implements Jogada {

	private static final long serialVersionUID = -4495406284719478720L;
	
	private String mensagem;
	
	public Message(String mensagem) {
		super();
		this.mensagem = mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getMensagem() {
		return mensagem;
	}

}
