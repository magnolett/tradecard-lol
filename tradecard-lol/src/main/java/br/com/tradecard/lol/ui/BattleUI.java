package br.com.tradecard.lol.ui;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.ufsc.inf.leobr.cliente.Proxy;

public class BattleUI extends JPanel {
	private static final long serialVersionUID = -6511706849835588388L;
	private String pathDefault = "zeds.jpg";
	private JButton jcomp1;
	private JButton jcompDesconectar;
	private JButton jcompIniciar;
    private JButton jcomp2;
    private JButton jcomp3;
    private JLabel jcomp4;
    private JButton jcomp5;
    private JButton jcomp6;
    private JButton jcomp7;
    private JButton jcomp8;
    private JButton jcomp9;
    private JButton jcomp10;
    private JLabel jcomp11;
    private JLabel jcomp12;
    private JLabel jcomp13;
    private JLabel jcomp14;
    private JButton jcomp15;
    private String selectedDeck;
    private AtorRede atorRede = new AtorRede(this);
    private String nome = "";
    
    public BattleUI() {}
    
    public BattleUI(String selectedDeck) throws FileNotFoundException, IOException {
    	this.nome = JOptionPane.showInputDialog("Informe o seu nome como participante da batalha!");
    	this.selectedDeck = selectedDeck;
        jcomp1 = new JButton ("Atacar");
        jcomp2 = new JButton ("Conectar");
        jcompIniciar = new JButton ("Iniciar Partida");
        jcompDesconectar = new JButton ("Desconectar");
        jcomp3 = new JButton ("Usar Habilidade");
        jcomp4 = new JLabel ("Nome do advers�rio: ");
        jcomp5 = new JButton ("Personagem 1");
        jcomp6 = new JButton ("Personagem 2");
        jcomp7 = new JButton ("Personagem 3");
        jcomp8 = new JButton ("Advers�rio 1");
        jcomp9 = new JButton ("Advers�rio 2");
        jcomp10 = new JButton ("Advers�rio 3");
        jcomp11 = new JLabel ("Sua vida:");
        jcomp12 = new JLabel ("Vida do advers�rio: ");
        jcomp13 = new JLabel ("VERSUS");
        jcomp14 = new JLabel ("Twisted Treeline 3x3");
        jcomp15 = new JButton ("Desistir");

		JLabel imageLabel = new JLabel();
		imageLabel.setSize(new Dimension (944, 574));
		
		ImageIcon imageIcon = new ImageIcon(getClass().getClassLoader().getResource(pathDefault));
		imageLabel.setIcon(imageIcon);
        
        setPreferredSize (new Dimension (943, 574));
        setLayout (null);

        add (jcomp1);
        add (jcompDesconectar);
        add (jcompIniciar);
        add (jcomp2);
        add (jcomp3);
        add (jcomp4);
        add (jcomp5);
        add (jcomp6);
        add (jcomp7);
        add (jcomp8);
        add (jcomp9);
        add (jcomp10);
        add (jcomp11);
        add (jcomp12);
        add (jcomp13);
        add (jcomp14);
        add (jcomp15);
        add(imageLabel);

        jcomp1.setBounds (65, 355, 100, 20);
        jcomp2.setBounds (5, 10, 100, 20);
        jcompDesconectar.setBounds (125, 10, 120, 20);
        jcompIniciar.setBounds (125, 35, 120, 20);
        jcomp3.setBounds (275, 355, 125, 20);
        jcomp4.setBounds (395, 460, 300, 25);
        jcomp5.setBounds (15, 85, 120, 175);
        jcomp6.setBounds (150, 85, 120, 175);
        jcomp7.setBounds (285, 85, 120, 175);
        jcomp8.setBounds (520, 85, 120, 175);
        jcomp9.setBounds (660, 85, 120, 175);
        jcomp10.setBounds (800, 85, 120, 175);
        jcomp11.setBounds (185, 410, 100, 25);
        jcomp12.setBounds (600, 410, 180, 25);
        jcomp13.setBounds (440, 150, 55, 40);
        jcomp14.setBounds (400, 25, 130, 25);
        jcomp15.setBounds (5, 35, 100, 20);
        
        jcomp1.setOpaque(true);
        jcomp2.setOpaque(true);
        jcomp3.setOpaque(true);
        jcomp4.setOpaque(true);
        jcomp5.setOpaque(true);
        jcomp6.setOpaque(true);
        jcomp7.setOpaque(true);
        jcomp8.setOpaque(true);
        jcomp9.setOpaque(true);
        jcomp10.setOpaque(true);
        jcomp11.setOpaque(true);
        jcomp12.setOpaque(true);
        jcomp13.setOpaque(true);
        jcomp14.setOpaque(true);
        jcomp15.setOpaque(true);
        
        characterButtons();
        
        jcomp2.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				atorRede.conectar(nome, "localhost");
				try {
					JOptionPane.showMessageDialog(null, "Conectado com sucesso!");
				} catch (HeadlessException e1) {
					e1.printStackTrace();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
        
        jcompIniciar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String obterNomeAdversario = "";
				try {
					obterNomeAdversario = atorRede.obterNomeAdversario();
					atorRede.iniciarPartidaRede();
					JOptionPane.showMessageDialog(null,  "Advers�rio ativo: " + obterNomeAdversario);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "Erro! N�o � possivel iniciar a partida sozinho!");
					e1.printStackTrace();
				}
			}
		});
        
        jcompDesconectar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				atorRede.desconectar();
				JOptionPane.showMessageDialog(null, "Desconectado com sucesso!");
			}
		});
        
        jcomp1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Aqui ser�o inseridas as op��es de ataque!");
			}
		});
        
        jcomp2.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Conectando a localhost...");
			}
		});
        
        jcomp3.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Aqui ser�o inseridas as habilidades dispon�veis!");
			}
		});
        
        jcomp15.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Voc� � um desistente!");
				
			}
		});
    }
    

    public ActionListener characterListener() {
    	return new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Aqui ser�o inseridas as a��es de personagens!");
			}
		};
    }
    
    public void characterButtons() {
    	ActionListener characterListener = characterListener();
    	
    	jcomp5.addActionListener(characterListener);
    	jcomp6.addActionListener(characterListener);
    	jcomp7.addActionListener(characterListener);
    	jcomp8.addActionListener(characterListener);
    	jcomp9.addActionListener(characterListener);
    	jcomp10.addActionListener(characterListener);
    }
    
    public void createScreen(String selectedDeck) throws FileNotFoundException, IOException {
    	JFrame frame = new JFrame ("Campo de batalha!");
    	frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    	frame.getContentPane().add (new BattleUI(selectedDeck));
    	frame.pack();
        frame.setLocationRelativeTo(null);
    	frame.setVisible(true);
    }
    
	public void receberMensagemRede(String mensagem) {
		// TODO Auto-generated method stub
		
	}

	public void iniciarPartidaRede(boolean comecoJogando) {
		try {
			String nomeAdversario = atorRede.obterNomeAdversario();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void iniciarJogo() {
		atorRede.iniciarPartidaRede();
	}

//    public static void main (String[] args) throws FileNotFoundException, IOException {
//        JFrame frame = new JFrame ("Campo de batalha!");
//        frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
//        frame.getContentPane().add (new BattleUI());
//        frame.pack();
//        frame.setLocationRelativeTo(null);
//        frame.setVisible(true);
//    }
}
